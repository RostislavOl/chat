/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.videoChat.chat.controllers;

import com.videoChat.chat.entities.AudioStream;
import com.videoChat.chat.entities.Message;
import com.videoChat.chat.entities.Session;
//import com.videoChat.chat.entities.User;
import com.videoChat.chat.entities.VideoStream;
//import com.videoChat.chat.entities.VideoStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.videoChat.chat.services.MessageService;
import com.videoChat.chat.services.OperationService;
import com.videoChat.chat.services.SessionService;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Rost
 */
@RestController
@RequestMapping(path = "/api")
@Slf4j
public class UserController {

    @Autowired
    private MessageService messageService;

    @Autowired
    private AudioStream audioStream;

    @Autowired
    private VideoStream videoStream;

    @Autowired
    private SessionService sessionService;

    private OperationService operations = new OperationService();

    /*@RequestMapping(method = RequestMethod.POST, path = "/auth/")
    public boolean authentification(@RequestBody User user){
        return validateService.acceptUser(user);
    }
    
    @RequestMapping(method = RequestMethod.POST, path = "/auth/videoConferenceRoom/{url}?{courcename}")
    public void setVideo(@PathVariable("url") String url, @PathVariable("courcename") String courcename){
        
    }
    
    @RequestMapping(method = RequestMethod.GET, path = "/auth/videoConferenceRoom/{url}")
    public OutputStream getVideo(){
        
        
        return null;
    }
    
    @RequestMapping(method = RequestMethod.POST, path = "/auth/videoConferenceRoom/{AudioSystem}")
    public void setVoice(@PathVariable("AudioSystem") AudioSystem audioSystem) throws LineUnavailableException{
        audioStream.read(audioSystem);
    }*/
    @RequestMapping(method = RequestMethod.POST, path = "/auth/chatRooms/")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> createChatRoom(@RequestBody Session session) {
        sessionService.addChatRoomAtList(session);
        return ResponseEntity.status(HttpStatus.OK).body(session);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/auth/chatRooms/")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> getChatRooms() {
        return ResponseEntity.status(HttpStatus.OK).body(sessionService.getSessionList());
    }
    
    @RequestMapping(method = RequestMethod.POST, path = "/auth/chatRooms/{id}/chatConferenceRoom/{userID}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> connectToChatRoom(@PathVariable ("roomID") UUID roomID, @PathVariable ("userID") UUID userID){
        if(sessionService.getChatRoom(roomID).getUserList().containsKey(userID)){
            return ResponseEntity.status(HttpStatus.OK).body("user " + userID + " was joined to session");
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("there is no user with " + userID + " id");
    }
    
    @RequestMapping(method = RequestMethod.DELETE, path = "/auth/chatRooms/{id}")
    public ResponseEntity<?> terminateSession(@PathVariable("id") UUID roomId) {
        sessionService.terminateChatRoom(roomId);
        return ResponseEntity.status(HttpStatus.OK).body("session " + roomId + " was terminated");
    }

    @RequestMapping(method = RequestMethod.GET, path = "/auth/chatRooms/{id}/chatConferenceRoom/{messageId}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> getMessages(@PathVariable("id") UUID roomID, @PathVariable("messageID") UUID messageID) {
        Message message = messageService.getMessageById(sessionService.getChatRoom(roomID).getMessageList(), messageID);
        return ResponseEntity.status(HttpStatus.OK).body(message);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/auth/chatRooms/{id}/chatConferenceRoom/")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> getMessages(@PathVariable("id") UUID roomID) {
        HashMap<UUID, Message> messages = messageService.getMessageList(sessionService.getChatRoom(roomID).getMessageList());
        return ResponseEntity.status(HttpStatus.OK).body(messages);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/auth/chatRooms/{id}/chatConferenceRoom/")
    public ResponseEntity<?> setMessage(@RequestBody Message message, @PathVariable("id") UUID roomID) throws Exception {
        message.setMessageTime(new Date().toString());
        if (sessionService.getChatRoom(roomID).getUserList().containsKey(message.getUserID())) {
            messageService.putMessage(sessionService.getChatRoom(roomID).getMessageList(), operations.generateId(), message);
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(message);
    }
}
