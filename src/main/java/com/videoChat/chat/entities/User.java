/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.videoChat.chat.entities;

import java.util.HashMap;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Rost
 */

public class User {
    
    @Getter
    @Setter
    private UUID id;
    
    @Getter
    @Setter
    private String name;
    
    @Getter
    @Setter
    private boolean isStudent;
    
    /*@Getter
    @Setter
    private HashMap<UUID, Message> userMessages;
    */
}
