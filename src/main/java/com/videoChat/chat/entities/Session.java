/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.videoChat.chat.entities;

import java.util.HashMap;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import java.util.Date;

/**
 *
 * @author rool0816
 */
public class Session {
    
    @Getter
    @Setter
    private Date sessionStartTime;
    
    @Getter
    @Setter
    private Date sessionEndTimePlan;
    
    @Getter
    @Setter
    private Date sessionEndTimeFact;
    
    @Getter
    @Setter
    private HashMap<UUID, User> userList;
    
    @Getter
    @Setter
    private HashMap<UUID, Message> messageList;
    
    public Session(HashMap<UUID, User> userList, HashMap<UUID, Message> messageList){
        this.userList = userList;
        this.messageList = messageList;
    }
    
    public Session(){}
    
    /*
    methods and fields for storage video and audio stream links
    */
    
}
