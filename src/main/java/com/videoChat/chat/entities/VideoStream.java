/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.videoChat.chat.entities;

import com.xuggle.xuggler.ICodec;
import com.xuggle.xuggler.IContainer;
import com.xuggle.xuggler.IContainerFormat;
import com.xuggle.xuggler.IPixelFormat;
import com.xuggle.xuggler.IStream;
import com.xuggle.xuggler.IStreamCoder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

/**
 *
 * @author Ростислав
 */
@Component
public class VideoStream {

    @Setter
    @Getter
    private static String url;
    private static int framesToEncode = 60;
    private static int x = 0;
    private static int y = 0;
    @Getter
    @Setter
    private static String fileName;
    //private static int height;
    //private static int width;

    public void createVideoStream() {
        IContainer container = IContainer.make();
        IContainerFormat containerFormat_live = IContainerFormat.make();
        containerFormat_live.setOutputFormat("flv", url + getFileName(), null);
        container.setInputBufferLength(0);
        int retVal = container.open(url + getFileName(), IContainer.Type.WRITE, containerFormat_live);
        if (retVal < 0) {
            System.err.println("Could not open output container for live stream");
            System.exit(1);
        }
        IStream stream = container.addNewStream(0);
        IStreamCoder coder = stream.getStreamCoder();
        ICodec codec = ICodec.findEncodingCodec(ICodec.ID.CODEC_ID_H264);
        coder.setNumPicturesInGroupOfPictures(5);
        coder.setCodec(codec);
        coder.setBitRate(200000);
        coder.setPixelType(IPixelFormat.Type.YUV420P);
        //coder.setHeight(height);
        //coder.setWidth(width);
        coder.setFlag(IStreamCoder.Flags.FLAG_QSCALE, true);
        coder.setGlobalQuality(0);
    }
}
