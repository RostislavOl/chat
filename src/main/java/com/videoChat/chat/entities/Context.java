/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.videoChat.chat.entities;

import java.io.InputStream;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Rost
 */
public class Context {
    
    @Getter
    @Setter
    private InputStream videoStream;
    
    @Getter
    @Setter
    private InputStream audioStream;
    
}
