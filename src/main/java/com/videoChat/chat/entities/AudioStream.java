/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.videoChat.chat.entities;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;
import org.springframework.stereotype.Component;

/**
 *
 * @author Ростислав
 */
@Component
public class AudioStream {

    volatile boolean finishFlag;
    static volatile byte[] data;
    static final Object monitor = new Object();
    static volatile Integer sendersCreated = 0;
    static volatile Integer numBytesRead;
    static volatile Integer senderNotReady = 0;

    AudioFormat format = new AudioFormat(16000.0f, 16, 2, true, false);
    //AudioFormat format = new AudioFormat(8000.0f, 8, 1, true, false);
    int CHUNK_SIZE = 10240;
    TargetDataLine microphone;

    public AudioStream() {
        finishFlag = false;
    }

    public void read(AudioSystem as) throws LineUnavailableException {
        microphone = as.getTargetDataLine(format);
        DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
        microphone = (TargetDataLine) as.getLine(info);
        microphone.open(format);

        data = new byte[CHUNK_SIZE];
        microphone.start();

        while (!finishFlag) {
            synchronized (monitor) {
                if (senderNotReady == sendersCreated) {
                    monitor.notifyAll();
                    continue;
                }

                numBytesRead = microphone.read(data, 0, CHUNK_SIZE);
            }
        }
    }
}
