/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.videoChat.chat.entities;

import java.util.UUID;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Rost
 */
public class Message {
    
    @Getter
    @Setter
    private String userName;
    
    @Getter
    @Setter
    private UUID userID;
    
    @Getter
    @Setter
    private String text;
    
    @Getter
    @Setter
    private boolean flag;
    
    @Getter
    @Setter
    private String messageTime;
    
    public Message(String userName, String text, boolean flag, String messageTime, UUID userID){
        this.messageTime = messageTime;
        this.userName = userName;
        this.text = text;
        this.flag = flag;
    }
    
    public Message(){}
    
}
