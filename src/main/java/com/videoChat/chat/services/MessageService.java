/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.videoChat.chat.services;

import com.videoChat.chat.entities.Message;
import java.util.HashMap;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 *
 * @author rool0816
 */
@Service
@Component
@Slf4j
public class MessageService {
      
    @Getter
    @Setter
    Message message;
    
    public Message getMessageById(HashMap<UUID, Message> messages, UUID id){
        return messages.get(id);
    }
    
    public HashMap getMessageList(HashMap<UUID, Message> messages){
        return messages;
    }
    
    public MessageService(){
        
    }
    
    public HashMap putMessage(HashMap<UUID, Message> messages, UUID messageId, Message message){
        messages.put(messageId, message);
        return messages;
    }
    
    /*private void redact(long messageid, Message message){
        for(int i = 0; i < messages.size(); i++){
            if(messages.get(i).equals(message))
                
        }
    }*/
    
}
