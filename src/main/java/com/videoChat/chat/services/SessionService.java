/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.videoChat.chat.services;

import com.videoChat.chat.entities.Session;
import java.util.HashMap;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;


/**
 *
 * @author rool0816
 */
@Service
@Component
public class SessionService {
    
    private OperationService operations = new OperationService();
    
    @Getter
    @Setter
    private HashMap<UUID, Session> sessionList;
    
    public SessionService(){
        sessionList = new HashMap<UUID, Session>();
    }
    
    public Session getChatRoom(UUID id){
        return sessionList.get(id);
    }
    
    public HashMap addChatRoomAtList(Session session){
        UUID roomId = operations.generateId();
        sessionList.put(roomId, session);
        return sessionList;
    }
    
    public void terminateChatRoom(UUID roomID){
        sessionList.remove(roomID);
    }
}
